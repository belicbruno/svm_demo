from svm_classifier import load_svm
from data import get_embeddings, load_data, get_data
from torch import LongTensor, mean
import numpy as np

def test_examples(svm):
    """
        Test an svm model on self specified examples.
    """
    embeddings_matrix, stoi = get_embeddings()

    examples = ['He is a liar',
                'I failed my exam',
          'I like roses',
          'This is a good sentance',
          'Nice job',
          'This is neither here nor there',
          'Get out of there',
          'You are too good to be true',
          'You are so bad that it turns the page around',
          'There is too much candy',
          'Love is in the air'
          ]
    for example in examples:
         numericalized = [stoi[x] for x in example.lower().split()]
         embedding = embeddings_matrix(LongTensor(numericalized))
         features = mean(embedding, dim=0).numpy().reshape(1, -1)
         y_p = svm.predict(features)
         prediction = 'positive' if y_p[0] else 'negative'
         print('{}  -  {}\n'.format(example, prediction))

def test_dataset(svm, split):
    """
        Test an SVM model on 20 randomly selected examples from a given
        split.
    """
    data = load_data(split)
    _, _, X_test, y_test = get_data()
    idx = np.arange(X_test.shape[0])
    np.random.shuffle(idx)
    y_p = svm.predict(X_test[idx[:20]])
    correct = 0
    for i, y_pi in enumerate(y_p):

        if y_pi == 1 and data.iloc[idx[i], 1].strip() == 'positive' \
            or y_pi == 0 and data.iloc[idx[i], 1].strip() == 'negative':
            correct += 1

        print('{} \nprediction: {} gt: {}\n'.format(data.iloc[idx[i], 0], y_pi,
                                                    data.iloc[idx[i], 1]))

    print('Accuracy: {}'.format(correct/20))




if __name__ == '__main__':
    svm = load_svm()
    test_examples(svm)
#    test_dataset(svm, 'test')
