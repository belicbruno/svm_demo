from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
import numpy as np
import data
from sklearn.metrics import classification_report
import torch
import joblib


def get_svm(X, y, parameters):
    """
        Return the SVM model which yields best results on a given dataset
        (X, y). Model is found in the space of given parameter dictionary
        using grid search.
    """
    grid_search = GridSearchCV(SVC(class_weight='balanced'), parameters, cv=5)
    grid_search.fit(X, y)

    print("Best score: %0.3f" % grid_search.best_score_)
    print("Best parameters set:")
    best_parameters = grid_search.best_estimator_.get_params()
    for param_name in sorted(parameters.keys()):
        print("\t%s: %r" % (param_name, best_parameters[param_name]))

    return grid_search.best_estimator_

def train_svm(X, y):
    """
        Train a predefined single SVM model on given dataset (X, y).
    """
    svm = SVC(C=10, kernel='linear')
    #svm = SVC(C=2, kernel='rbf', gamma=1)
    svm.fit(X, y)

    return svm

def load_svm():
    """
        Load a saved SVM model.
    """
    return joblib.load('svm_model.save')



def main():

    X_train, y_train, X_test, y_test = data.get_data()

    # of next three lines of code, only one should be uncommented at a time
    # each is used to obtain the SVM model a different way

    #svm = get_svm(X, y, {'C' : (0.5, 2, 5, 10), 'kernel' : ['linear', 'poly', 'rbf']})
    svm = train_svm(X_train, y_train)
    #svm = load_svm()

    print('Train:\n', classification_report(y_train, svm.predict(X_train)))
    print('Test:\n', classification_report(y_test, svm.predict(X_test)))
    joblib.dump(svm, 'svm_model.save') # comment if you dont want to save SVM

if __name__ == '__main__':
    main()
