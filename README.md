# README #

This is a demonstration of an SVM classifier.
Each branch is here for the purpuse of demonstrating different properties of the classifier,
with the sentiment_analysis branch being a final product.

### Branches ###

* random_data - demonstrates hiperparameters usage on random data
* sentiment_analysis - sentance positivity/negativity prediction trained on the IMDB dataset

### What do you need? ###

* Python 3.*
* sklearn
* pytorch
* numpy
* pandas

