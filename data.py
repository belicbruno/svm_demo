import pandas as pd
import os
from torchtext.data import Field, Iterator, Dataset, Example
import torch.nn as nn
import torch
import numpy as np

dataset_path = 'data'

def load_data(split):
    """
        Loads data from a globally specified dataset path.
        Use argument split to specify for train, validation or test split of the
        dataset.
    """
    instances = pd.read_csv(os.path.join(dataset_path,
                                         'sst_{}_raw.csv'.format(split)),
                                         header=None)
    return instances

def get_text_field(text):
    """
        Specifies a text field for a custom Dataset class.
    """
    field = Field(
        tokenize='basic_english',
        lower=True
    )
    text = [x.split() for x in text]
    field.build_vocab(
            text,
            vectors='glove.6B.300d') # builds a vocabulary from all of the words
                                     # in the given data
    return field

def preprocess_label(label):
    """
        Convert from positive-negative to 0-1 labelling.
    """
    return 1 if label.strip() == 'positive' else 0

def get_label_field():
    """
        Specifies a label field for a custom Dataset class.
    """
    field = Field(sequential=False, use_vocab=False, preprocessing=preprocess_label)
    return field

class IMDBDataset(Dataset):
    """
        Custom dataset class.
    """
    def __init__(self, data, fields):
        e = [
                Example.fromlist(list(r), fields)
                for i, r in data.iterrows()
            ]
        super(IMDBDataset, self).__init__(e, fields)

    def __len__(self):
        return len(self.examples)

def get_dataset(data, text_field, label_field):
    """
        Get dataset from data, specified text and label field objects.
    """
    fields = [('text',text_field), ('label',label_field)]
    dataset = IMDBDataset(data, fields)
    return dataset

def get_iterator(dataset, batch_size, train):
    """
        Get iterator for a given dataset.
        This has no real use in this project, it is simply added since all of
        these classes are optimized for batch-learning used in neural network
        approaches.
    """
    iterator = Iterator(
        dataset=dataset, sort_key=lambda x: len(x.text),
        batch_size=batch_size, shuffle=False, train=train
    )
    return iterator

def get_iterators():
    """
        Returns a pair (list of iterators, text field).
        List of iterators contains iterators for all of the dataset splits,
        while text field contains a vocabulary built from all of the words in
        the train set.
    """
    iters = []
    for set in ['train', 'valid', 'test']:
        data = load_data(set)
        if set == 'train':
            text_field = get_text_field(list(data.iloc[:, 0]))
        label_field = get_label_field()
        dataset = get_dataset(data, text_field, label_field)
        iters.append(get_iterator(dataset, data.size, set == 'train'))
    return (iters, text_field)

def get_data():
    """
        Wrapper which uses above IMDBDataset class to generate feature matrix
        by embedding all of the words and averaging them for each of the
        example sentances.
        Does so for train and test set, and returns X, y for each of them.
    """
    (train_data, valid_data, test_data), text_field = \
        get_iterators()
    pre_trained_emb = torch.FloatTensor(text_field.vocab.vectors)
    embedding = nn.Embedding.from_pretrained(pre_trained_emb)

    examples = next(iter(train_data))
    X_train = torch.mean(embedding(examples.text), dim=0).numpy()
    y_train = examples.label.numpy()
    examples = next(iter(test_data))
    X_test = torch.mean(embedding(examples.text), dim=0).numpy()
    y_test = examples.label.numpy()
    return X_train, y_train, X_test, y_test

def get_embeddings():
    """
        Return an embedding matrix generated from the train set vocabulary.
    """
    _, text_field = get_iterators()
    pre_trained_emb = torch.FloatTensor(text_field.vocab.vectors)
    return nn.Embedding.from_pretrained(pre_trained_emb), text_field.vocab.stoi




if __name__ == '__main__':
    X, y = get_data(6919)
    print (np.sum(y)/6919)
